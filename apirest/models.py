from django.db import models

"""[summary]
Se crea el modelo Mascota con los campos requeridos
[description]
"""
class Mascota(models.Model):
    nombre = models.CharField(max_length=50)
    tipo = models.CharField(max_length=10)
    fecha_nacimiento = models.DateField()

    class Meta:
        db_table = 'mascotas'
        order_with_respect_to = 'nombre'