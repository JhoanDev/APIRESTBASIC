from rest_framework import serializers
from apirest.models import Mascota


"""[summary]
Se define el serializador para el modelo Mascota con los respectivos campos
[description]
HyperlinkedModelSerializer => Esto permite entregar por medio del campo "url" especificado
en "fields" una url completa "http://127.0.0.1:8000/mascotas/2/" y no relativa "mascotas/2/"
"""

class MascotaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Mascota
        fields = ('url', 'nombre', 'tipo', 'fecha_nacimiento')
