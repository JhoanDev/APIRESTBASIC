from rest_framework import viewsets
from apirest.serializers import MascotaSerializer
from apirest.models import Mascota

"""[summary]
[description]
Punto final de la API que permite que los usuarios puedan ser vistos o editados.
"""
class MascotaViewSet(viewsets.ModelViewSet):
    queryset = Mascota.objects.all().order_by('id')
    serializer_class = MascotaSerializer