from django.conf.urls import url, include
from rest_framework import routers
from apirest import views

router = routers.DefaultRouter()
router.register(r'mascotas', views.MascotaViewSet)

"""[summary]

[description]
# Conecta nuestra API usando el enrutamiento automático de URL.
# Además, incluimos las URL de inicio de sesión para la API navegable.
"""
urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]